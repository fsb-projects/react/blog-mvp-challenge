import React from 'react'
import FormComponent from '../components/FormComponent'
import * as yup from 'yup';
import {Formik} from 'formik'
import {createNewPost} from '../redux/actions/allPosts-actions'
import {connect} from 'react-redux'

const validations=yup.object().shape({
    tags:yup.array(),
    tag:yup.string(),
    title:yup.string()
    .max(30,"Can`t be more than 30 characters")
    .required("Must not be empty"),
    description:yup.string()
    .required("The description is necessary"),
    file:yup.mixed().required("You must provide a picture")
})

const PostFormContainer=({createNewPost,history})=>{
    const handleSubmit=(values)=>{
        let image= new FormData()
        image.append("image",values.file,values.file.name)
        createNewPost({body:{...values},image})
        .then(status=>history.push(status.id?`/posts/${status.id}`:`/posts`))
    }

    return(
        <Formik
        initialValues={{
            tags:[],
            tag:"",
            title:"",
            description:"",
            file:null
        }}
        onSubmit={handleSubmit}
        validationSchema={validations}
        component={FormComponent}
        >
        </Formik>
    )
}

const mapDispatchToProps=(dispatch)=>({
    createNewPost:(body)=>dispatch(createNewPost(body))
})

export default connect(null,mapDispatchToProps)(PostFormContainer)