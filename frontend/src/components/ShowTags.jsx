import React from 'react'
import PropTypes from 'prop-types'

import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    chip: {
        margin: theme.spacing(1),
      }
}));
const ShowTags= ({tags,handleDeleteTag, edit})=>{
    const classes=useStyles()

    return(
        <Grid container justify="flex-start" alignItems="center">
            {tags && tags.length?
                 tags.map((tag,index)=><Chip type="button"
                     label={tag}
                     key={tag}
                     onDelete={edit?()=>handleDeleteTag(index):null}
                     className={classes.chip}
                   />)
            :null}
        </Grid>
    )
}
ShowTags.propTypes={

}
export default ShowTags