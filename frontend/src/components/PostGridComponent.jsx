import React from 'react'
import PropTypes from 'prop-types'

import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import PostPreview from './PostPreview';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(theme => ({
      cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
      }
  }));

const PostGridComponent= ({posts})=>{
    const classes=useStyles()

return (
    <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
            {posts.map(post => (
                <PostPreview key={post.id} {...post}/>
            ))}
        </Grid>
  </Container>
)
}
PostGridComponent.propTypes={
posts:PropTypes.arrayOf(
    PropTypes.object
)
}
export default PostGridComponent