import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Form} from 'formik'
import Body from './Body'
import Tags from './Tags';
import FormImage from './FormImage';

const useStyles = makeStyles(theme => ({
    container:{
        width:"80vw",
      padding:"5vw",
      margin:"0 auto"
    },
    loader:{
        backgroundColor:"#8fb3f1"
    },
}));

const PostForm=({ values, handleSubmit, setFieldValue,setFieldError,touched,errors }) => {
    const handleTagSubmit=()=>{
        if(values.tag && 
          values.tags.indexOf(values.tag.toLowerCase())==-1){
              setFieldValue("tags",[...values.tags,values.tag])
              setFieldValue("tag","")
        }
        setFieldError("tag","Can`t be empty or repeated")
    }
    const handleDeleteTag=(index)=>{
        setFieldValue("tags",[...values.tags.slice(0,index),...values.tags.slice(index+1)])
    }
    const handleFile=(event) => {
        setFieldValue("file", event.currentTarget.files[0]);
      }
    const classes=useStyles()
    return (
        <Form onSubmit={handleSubmit}>
            <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
                className={classes.container}
                spacing={2}>
                <Body/>
                <Grid item xs={12} sm={4}>
                    <FormImage file={values.file} 
                        handleFile={handleFile} 
                        form={{touched,errors}}/>
                </Grid>
                <Tags handleTagSubmit={handleTagSubmit} 
                    handleDeleteTag={handleDeleteTag}
                    values={values}/>
                <Grid item xs={12}>
                    <Button 
                        type="submit"
                        variant="contained" 
                        color="primary">
                        Enviar
                    </Button>
                </Grid>
            </Grid>
        </Form>
    )
}





export default PostForm
