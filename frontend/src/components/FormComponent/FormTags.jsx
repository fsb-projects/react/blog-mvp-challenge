import React from 'react'
import PropTypes from 'prop-types'

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Error from './Error'
import ShowTags from '../ShowTags'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
      button: {
        margin: theme.spacing(1),
      }
}));
const FormTags= ({tags,
                handleTagSubmit,
                handleDeleteTag})=>{
    const classes=useStyles()

    return(
        <>
        <Grid container justify="space-between" alignItems="center">
            <Grid item xs={12} sm={8}>
                <Text />
            </Grid>
            <Grid item xs={6} sm={4}>
                <Button 
                    variant="contained" 
                    color="primary"
                    size="small"
                    className={classes.button}
                    onClick={handleTagSubmit}>
                    Add Tag
                </Button>
            </Grid>
        </Grid>
        <ShowTags 
            tags={tags} 
            handleDeleteTag={handleDeleteTag} 
            edit={true}
        />
    </>
    )
}
FormTags.propTypes={
    values:PropTypes.shape({
        tags:PropTypes.arrayOf(PropTypes.string)
    }
    ),
    handleTagSubmit:PropTypes.func,
    handleDeleteTag:PropTypes.func,
}
export default FormTags