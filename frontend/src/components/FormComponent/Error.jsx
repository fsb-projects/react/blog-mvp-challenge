import React from 'react'
import PropTypes from 'prop-types'

const Error= ({touched,errors,name})=>{
    return touched[name] &&
        errors[name]? 
        <small
            style={{color:"red"}}>
            {errors[name]}
        </small>
        :null
}


Error.propTypes={
name:PropTypes.string,
touched:PropTypes.object,
errors:PropTypes.object
}
export default Error