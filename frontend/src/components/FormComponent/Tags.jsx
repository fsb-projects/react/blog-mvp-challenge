import React from 'react'
import { Field} from 'formik'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Text from './Text'
import ShowTags from '../ShowTags'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
      }
}));

export default ({handleTagSubmit,handleDeleteTag,values})=>{
    const classes=useStyles()
    return (<Grid item xs={12} sm={6}>
        <Grid container justify="space-between" alignItems="center">
            <Grid item xs={12} sm={8}>
            <Field type="text" name="tag" placeholder="Post`s tag" component={Text}/>
            </Grid>
            <Grid item xs={6} sm={4}>
                <Button 
                    variant="contained" 
                    color="primary"
                    size="small"
                    type="button"
                    className={classes.button}
                    onClick={handleTagSubmit}>
                    Add Tag
                </Button>
            </Grid>
        </Grid>
        <ShowTags 
            tags={values.tags} 
            handleDeleteTag={handleDeleteTag} 
            edit={true}/>
    </Grid>)
}