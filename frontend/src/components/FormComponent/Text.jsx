import React from 'react';
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';
import Error from './Error'


const Text= ({field,form,...props})=>(
            <TextField
                label={field.name.toUpperCase()}
                helperText={<Error {...form} name={field.name}/>}
                fullWidth
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                shrink: true,
                }}
                {...field}
                {...props}
            />
    )
Text.propTypes={
    field:PropTypes.object,
    form:PropTypes.object
}
export default Text
