import React from 'react'
import Text from './Text'
import { Field} from 'formik'
import Grid from '@material-ui/core/Grid';

export default ()=><Grid item xs={12} sm={8}>
                        <Field type="text" name="title" 
                            placeholder="Post`s Title" component={Text} />
                        <Field type="text" 
                            name="description" 
                            rows={3} 
                            multiline={true}
                            placeholder={"Post`s description"}
                            component={Text} />
                    </Grid>