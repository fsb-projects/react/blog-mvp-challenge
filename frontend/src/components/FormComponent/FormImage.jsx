import React, {useEffect,useState} from 'react';
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Error from './Error'
import { makeStyles } from '@material-ui/core/styles';
import { Field} from 'formik'

const useStyles = makeStyles(theme => ({
image:{
    maxWidth:"100%",
    margin:"0 auto",
    display:"block",
    marginTop:theme.spacing(2),
    marginBottom:theme.spacing(2),
    maxHeight:"30vh"
},

inputFile:{
    visibility:"hidden",
    position:"absolute"
}
}));

const FormImage=({file,handleFile,form})=>{
    const classes=useStyles()
    const [image,setImage]=useState("https://www.cvjp.org.ni/assets/no-image.png")
    //this allow us to display the image and executes every time the file changes
    useEffect(
        ()=>{
            if(file && file.name){
                let reader = new FileReader();
            reader.onload = function (e) {
                setImage(e.target.result)
            };
            reader.readAsDataURL(file)
            }
        },[file]
    )
    const handleUploadImage=()=>{
        document.getElementById("inputFile").click()
    }
    return (
        <>  
            <input id="inputFile"
            name="file"
            className={classes.inputFile} 
            type="file"
            onChange={handleFile}/>
            <Grid 
                container direction="row"
                justify="space-between"
                alignItems="flex-start"
                >
                <Typography variant="body2" color="textSecondary" component="p">
                    {`image: ${file?file.name:""}`}
                </Typography>
                <Button variant="contained" 
                    color="primary" 
                    className={classes.button}
                onClick={handleUploadImage}>
                     UPLOAD
                </Button>
            </Grid>
            <img src={image} 
                id="fileImg" 
                className={classes.image} 
                alt={file?file.name:"none"}
            />
            <br/>
            <Error {...form} name="file"/>
        </>
    )
}

FormImage.propTypes={
    file:PropTypes.object,
    form:PropTypes.object,
    handleSelectImage:PropTypes.func
}
export default FormImage
